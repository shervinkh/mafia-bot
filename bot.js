const Telegraf = require('telegraf')
const session = require('telegraf/session')
const Markup = require('telegraf/markup')
const Stage = require('telegraf/stage')
const Scene = require('telegraf/scenes/base')
const { leave } = Stage

// Roles Metadata
const rolesMeta = {
  don: {
    name: 'دان',
    group: 'mafia',
    team: 'mafia',
    stealth: true,
    don: true,
    choose: 'any',
  },
  terrorist: {
    name: 'تروریست',
    group: 'mafia',
    isTerrorist: true,
    team: 'mafia',
    choose: 'any',
  },
  mafia: {
    name: 'مافیا',
    group: 'mafia',
    team: 'mafia',
    choose: 'any',
  },
  doctor: {
    name: 'پزشک',
    group: 'doctor',
    team: 'citizen',
    choose: 'any',
  },
  detective: {
    name: 'کارآگاه',
    group: 'detective',
    team: 'citizen',
    choose: 'others',
  },
  sniper: {
    name: 'اسنایپر',
    group: 'sniper',
    team: 'citizen',
    choose: 'optional&others',
    shots: 2,
  },
  cupbearer: {
    name: 'ساقی',
    group: 'cupbearer',
    team: 'citizen',
    choose: 'norepeat&others',
  },
  silencer: {
    name: 'ناتاشا',
    group: 'silencer',
    team: 'citizen',
    choose: 'norepeat',
  },
  freemason: {
    name: 'فراماسون',
    group: 'freemason',
    team: 'citizen',
  },
  fool: {
    name: 'جوکر',
    group: 'fool',
    team: 'citizen',
  },
  invincible: {
    name: 'رویین‌تن',
    group: 'invincible',
    team: 'citizen',
  },
  citizen: {
    name: 'شهروند',
    group: 'citizen',
    team: 'citizen',
  },
}

// Global Database
db = {}
const getRoomTemplate = () => ({
  dead: [],
  players: {}, // ID to Name
  availableRoles: [],
  roles: {}, // ID to Role
  sniperShots: {},
  ctx: {}, // ID to ctx
  selections: {}, // Night Selections 'mafia': id
  votes: {}, // Votes in vote room
  admin: null,
})

// Helper Functions
function shuffle(a) {
  var j, x, i
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1))
    x = a[i]
    a[i] = a[j]
    a[j] = x
  }
}

function join(ctx, room) {
  if (!(room in db)) {
    ctx.reply(`اتاق ${room} وجود نداره.`)
    return
  }
  if (Object.keys(db[room].players).length >= db[room].availableRoles.length) {
    ctx.reply(`جا نیست.`)
    return
  }
  let name = ctx.from.first_name
  if (ctx.from.last_name) name += ` ${ctx.from.last_name}`
  db[room].players[ctx.from.id] = name
  db[room].ctx[ctx.from.id] = ctx
  ctx.session.room = room
  ctx.reply(`به اتاق ${room} جوین شدی.`)
  ctx.scene.enter('waiting')
}

function choose(ctx, group, whos) {
  const room = ctx.session.room
  const candidates = new Set()
  for (const playerId of Object.keys(db[room].players)) {
    candidates.add(playerId)
  }
  whos.split('&').forEach((who) => {
    if (who === 'others') candidates.delete(ctx.from.id.toString())
    if (who === 'optional') candidates.add(0)
    if (who === 'norepeat') {
      const prevSelection = db[room].ctx[ctx.from.id].session.lastSelection
      if (prevSelection && candidates.has(prevSelection)) {
        candidates.delete(prevSelection)
      }
    }
  })
  ctx.session.choices = Array.from(candidates).map((id) => [id, (id === 0) ? 'هیچ‌کس' : db[room].players[id]])
  ctx.session.choiceGroup = group

  ctx.reply('انتخاب کن.',
    Markup.keyboard(ctx.session.choices.map(([_, name]) => name))
    .oneTime()
    .resize()
    .extra()
  )
}

function applyChoice(ctx, msg, choices) {
  if (!ctx.session.choices) return false

  let choice = null
  for (const [id, name] of ctx.session.choices) {
    if (msg === name) {
      choice = id
      break
    }
  }
  if (choice !== null) {
    choices[ctx.session.choiceGroup] = choice
    return true
  } else return false
}

function removeChoices(ctx, msg) {
  ctx.session.choices = []
  ctx.session.choiceGroup = null
  ctx.reply(msg || 'پایان فاز انتخاب.', Markup.removeKeyboard().extra())
}

function endGame(room, winner) {
  const remainingRoles = Object.keys(db[room].players).map((id) => `${rolesMeta[db[room].roles[id]].name}: ${db[room].players[id]}`).join('\n')
  const report = `بازی اتاق ${room} تموم شد و <b>${rolesMeta[winner].name}</b> برنده شد.\n\nنقش افراد باقی‌مانده:\n${remainingRoles}`
  Object.keys(db[room].players).concat(db[room].dead).forEach((id) => {
    bot.telegram.sendMessage(id, report, {parse_mode: 'HTML'})
    db[room].ctx[id].session.room = null
    db[room].ctx[id].scene.leave()
  })
  delete db[room]
}

function kill(room, victims) {
  victims.forEach((id) => {
    delete db[room].players[id]
  })
  victims.forEach((id) => {
    //const remainingRoles = Object.keys(db[room].players).map((id) => `${rolesMeta[db[room].roles[id]].name}: ${db[room].players[id]}`).join('\n')
    db[room].ctx[id].reply(`تو کشته شدی و از بازی خارج شدی.`)//\n\nنقش‌های باقی‌مانده:\n${remainingRoles}`)
    db[room].dead.push(id)
    db[room].ctx[id].scene.leave()
    delete db[room].roles[id]
  })

  // Check end condition
  const remainingRoles = Object.values(db[room].roles).map((role) => rolesMeta[role].team)
  const citizens = remainingRoles.filter((team) => team === 'citizen').length
  const mafia = remainingRoles.filter((team) => team === 'mafia').length
  if (mafia === 0) setTimeout(() => endGame(room, 'citizen'), 0)
  else if (mafia >= citizens) setTimeout(() => endGame(room, 'mafia'), 0)
}

function aliveGroups(room) {
  const roles = new Set(Object.values(db[room].roles).filter((role) => rolesMeta[role].choose).map((role) => rolesMeta[role].group))
  return Array.from(roles)
}

// Waiting Room
const waitingRoom = new Scene('waiting')
waitingRoom.enter((ctx) => {
  const room = ctx.session.room
  let roles = ''
  if (db[room].availableRoles.length) {
    roles = `\n\nنقش‌های بازی:\n${db[room].availableRoles.map(role => rolesMeta[role].name).join('، ')}`
  }
  ctx.replyWithHTML(`<b>اتاق انتظار</b>\n\nفعلاً اینجا صبر کن تا همه به اتاق جوین بشن و بازی شروع بشه.${roles}`)
  Object.values(db[room].ctx).forEach((playerCtx) => {
    if (playerCtx.from.id !== ctx.from.id) {
      playerCtx.reply(`بازیکن ${db[room].players[ctx.from.id]} به اتاق جوین شد.`)
    }
  })
  if (Object.keys(db[room].players).length === db[room].availableRoles.length) {
    Object.values(db[room].ctx).forEach((playerCtx) => {
      playerCtx.reply(`اتاق پر شد. آماده برای شروع.`)
    })
  }
})
waitingRoom.command('add', (ctx) => {
  const room = ctx.session.room
  if (db[room].admin !== ctx.from.id) {
    ctx.reply('ادمین باید این کار رو بکنه.')
    return
  }
  const args = ctx.message.text.split(' ')
  if (args.length < 2) {
    ctx.reply('داری اشتباه می‌زنی. برای مثال اگه می‌خوای یک مافیا اضافه کنی، این دستور رو به شکل /add مافیا استفاده کن.')
    return
  }
  let role = null
  for (const oneRole in rolesMeta) {
    if (rolesMeta[oneRole].name === args[1]) role = oneRole
  }

  if (!role) {
    ctx.reply(`نقش ${args[1]} وجود نداره.`)
    return
  }
  db[room].availableRoles.push(role)
  ctx.reply(`نقش اضافه شد. نقش‌های فعلی:\n${db[room].availableRoles.map(role => rolesMeta[role].name).join('، ')}`)
})
waitingRoom.command('remove', (ctx) => {
  const room = ctx.session.room
  if (db[room].admin !== ctx.from.id) {
    ctx.reply('ادمین باید این کار رو بکنه.')
    return
  }
  const args = ctx.message.text.split(' ')
  if (args.length < 2) {
    ctx.reply('داری اشتباه می‌زنی. برای مثال اگه می‌خوای یک مافیا حذف کنی، این دستور رو به شکل /remove مافیا استفاده کن.')
    return
  }
  let idx = null
  for (let i = 0; i < db[room].availableRoles.length; ++i) {
    if (rolesMeta[db[room].availableRoles[i]].name === args[1]) idx = i;
  }
  if (idx === null) {
    ctx.reply(`نقش ${args[1]} وجود نداره.`)
    return
  }
  db[room].availableRoles = db[room].availableRoles.slice(0, idx).concat(db[room].availableRoles.slice(idx + 1))
  ctx.reply(`نقش حذف شد. نقش‌های فعلی:\n${db[room].availableRoles.map(role => rolesMeta[role].name).join('، ')}`)
})
waitingRoom.command('begin', (ctx) => {
  const room = ctx.session.room
  if (db[room].admin !== ctx.from.id) {
    ctx.reply('ادمین باید این کار رو بکنه.')
    return
  }
  if (db[room].availableRoles.length !== Object.keys(db[room].players).length) {
    ctx.reply('تعداد بازیکن‌ها با تعداد نقش‌ها برابر نیست.')
    return
  }
  removeChoices(ctx, 'اوکی')

  // Give Roles
  shuffle(db[room].availableRoles)
  const players = Object.keys(db[room].players)
  for (let i = 0; i < db[room].availableRoles.length; ++i) {
    db[room].roles[players[i]] = db[room].availableRoles[i]
    db[room].ctx[players[i]].session.lastSelection = null
    if (db[room].roles[players[i]] === 'sniper') db[room].sniperShots[players[i]] = rolesMeta['sniper'].shots
  }
  for (let i = 0; i < db[room].availableRoles.length; ++i) {
    let roleMsg = `بازی شروع شد. نقش تو در این بازی <b>${rolesMeta[db[room].roles[players[i]]].name}</b> است.`
    if (rolesMeta[db[room].roles[players[i]]].team === 'mafia') {
      const mafiaTeam = Object.keys(db[room].players).filter((id) => rolesMeta[db[room].roles[id]].team === 'mafia').map((id) => `${rolesMeta[db[room].roles[id]].name}: ${db[room].players[id]}`).join('\n')
      roleMsg += `\n\nاعضای تیم مافیا:\n${mafiaTeam}`
    } else if (db[room].roles[players[i]] === 'freemason') {
      const freemasonTeam = Object.keys(db[room].players).filter((id) => db[room].roles[id] === 'freemason').map((id) => `${rolesMeta[db[room].roles[id]].name}: ${db[room].players[id]}`).join('\n')
      roleMsg += `\n\nاعضای تیم فراماسون:\n${freemasonTeam}`
    }
    db[room].ctx[players[i]].replyWithHTML(roleMsg)
    db[room].ctx[players[i]].scene.enter('discussion')
  }
})
waitingRoom.on('message', (ctx) => {
  const room = ctx.session.room
  Object.values(db[room].ctx).forEach((playerCtx) => {
    if (playerCtx.from.id !== ctx.from.id) {
      ctx.forwardMessage(playerCtx.chat.id, ctx.chat.id, ctx.message.message_id)
    }
  })
})

// Discussion Room
const discussionRoom = new Scene('discussion')
discussionRoom.enter((ctx) => {
  ctx.replyWithHTML(`<b>اتاق بحث</b>\n\nالان روز هست و بحث انجام میشه. برای شروع رای‌گیری فقط یکی از افراد /vote بزنه و برای شب شدن /night بزنه.`)
})
discussionRoom.command('vote', (ctx) => {
  const room = ctx.session.room
  db[room].votes = {}
  Object.keys(db[room].players).forEach((id) => {
    if (id !== db[room].selections['silencer']) {
      db[room].ctx[id].scene.enter('vote')
    }
  })
})
discussionRoom.command('night', (ctx) => {
  const room = ctx.session.room
  db[room].selections = {}
  Object.keys(db[room].players).forEach((id) => {
    db[room].ctx[id].scene.enter('night')
  })
})
discussionRoom.on('message', (ctx) => {
  const room = ctx.session.room
  if (ctx.from.id.toString() === db[room].selections['silencer']) {
    ctx.reply('هیس...')
    return
  }
  Object.keys(db[room].players).forEach((id) => {
    if (id !== ctx.from.id.toString()) {
      ctx.forwardMessage(id, ctx.chat.id, ctx.message.message_id)
    }
  })
  db[room].dead.forEach((id) => {
    ctx.forwardMessage(id, ctx.chat.id, ctx.message.message_id)
  })
})

// Vote Room
const voteRoom = new Scene('vote')
voteRoom.enter((ctx) => {
  ctx.replyWithHTML(`<b>اتاق رای‌گیری</b>\n\nافراد زنده و غیرمسکوت رای بدن به نظرشون کی مافیاس. بعد از رای‌دادن همه، نتیجه اعلام میشه.`)
  choose(ctx, ctx.from.id, 'any')
})
voteRoom.on('message', (ctx) => {
  const room = ctx.session.room
  if (applyChoice(ctx, ctx.message.text, db[room].votes)) {
    ctx.reply(`اوکی. ${ctx.message.text} انتخاب شد.`)
    let elligibleVoters = Object.keys(db[room].players).length
    if (db[room].selections['silencer'] && (db[room].selections['silencer'] in db[room].players)) {
      --elligibleVoters
    }
    if (Object.keys(db[room].votes).length === elligibleVoters) {
      let voteLog = ''
      const counter = {}
      for (const voter in db[room].votes) {
        const votee = db[room].votes[voter]
        voteLog += `بازیکن ${db[room].players[voter]} به ${db[room].players[votee]} رای داد.\n`
        counter[votee] = (counter[votee] || 0) + 1
      }
      const votes = Object.keys(counter).map((key) => [counter[key], key])
      votes.sort()
      const [majority_vote, majority_id] = votes[votes.length - 1]
      let toDie = null
      if (majority_vote > Object.keys(db[room].players).length / 2) toDie = majority_id
      if (toDie !== null) {
        voteLog += `\nبازیکن ${db[room].players[toDie]} با نقش ${rolesMeta[db[room].roles[toDie]].name} با رای اکثریت کشته شد.`
        if (db[room].roles[toDie] === 'terrorist' && db[room].selections['cupbearer'] !== toDie) voteLog += '\nاین بازیکن اکنون در حال ترور یکی از بازیکن هاست.'
      } else {
        voteLog += `\nهیچ‌کس رای اکثریت را نیاورد.`
      }
      Object.keys(db[room].players).forEach((id) => {
        removeChoices(db[room].ctx[id], voteLog)
        if (id != toDie && id != db[room].selections['silencer']) {
          db[room].ctx[id].scene.enter('discussion')
        }
      })
      db[room].dead.forEach((id) => {
        ctx.telegram.sendMessage(id, `گزارش رای‌گیری برای ارواح اتاق ${room}:\n\n${voteLog}`)
      })
      if (toDie !== null) {
        if (db[room].roles[toDie] === 'terrorist' && db[room].selections['cupbearer'] !== toDie) {
          db[room].ctx[toDie].scene.enter('terror')
        } else if (db[room].roles[toDie] === 'fool') {
          setTimeout(() => endGame(room, 'fool'), 0)
        } else {
          kill(room, [toDie])
        }
      }
    }
  }
})

// Night Room
const nightRoom = new Scene('night')
nightRoom.enter((ctx) => {
  ctx.replyWithHTML(`<b>اتاق شب</b>\n\nالان شب هست. افراد نقش‌دار می‌تونن با هم‌تیمی‌هاشون حرف بزنن و هدف مورد نظرشون رو انتخاب کنن. در صورت وجود و زنده بودن دان، انتخاب مافیا رو باید دان انجام بده.`)
  const room = ctx.session.room
  const role = rolesMeta[db[room].roles[ctx.from.id]]
  if (role.choose) {
    choose(ctx, role.group, role.choose)
  }
})
nightRoom.on('message', (ctx) => {
  const room = ctx.session.room
  const role = rolesMeta[db[room].roles[ctx.from.id]]
  if (db[room].roles[ctx.from.id] !== 'citizen') {
    Object.keys(db[room].players).forEach((id) => {
      const otherRole = rolesMeta[db[room].roles[id]]
      if (id !== ctx.from.id.toString() && otherRole.group === role.group) {
        ctx.forwardMessage(id, ctx.chat.id, ctx.message.message_id)
      }
    })
  }
  //db[room].dead.forEach((id) => {
    //ctx.forwardMessage(id, ctx.chat.id, ctx.message.message_id)
  //})
  const donAlive = Object.values(db[room].roles).indexOf('don') !== -1
  const canChoose = role.team !== 'mafia' || role.don || !donAlive
  if (canChoose && applyChoice(ctx, ctx.message.text, db[room].selections)) {
    Object.keys(db[room].players).forEach((id) => {
      const otherRole = rolesMeta[db[room].roles[id]]
      if (otherRole.group === role.group) {
        db[room].ctx[id].reply(`اوکی. ${ctx.message.text} انتخاب شد.`)
      }
    })
    ctx.session.lastSelection = db[room].selections[role.group]
    if (Object.keys(db[room].selections).length >= aliveGroups(room).length) {
      const drunk = db[room].selections['cupbearer']

      let docProtected = null
      if (db[room].selections['doctor'] && db[room].roles[drunk] !== 'doctor') docProtected = db[room].selections['doctor']

      let mafiaShot = db[room].selections['mafia']
      let sniperShot = db[room].selections['sniper']
      let silencerShot = db[room].selections['silencer']

      let sniper = null
      let silencer = null
      let detective = null
      let invincible = null
      for (const playerId in db[room].players) {
        if (db[room].roles[playerId] === 'sniper') sniper = playerId
        if (db[room].roles[playerId] === 'silencer') silencer = playerId
        if (db[room].roles[playerId] === 'detective') detective = playerId
        if (db[room].roles[playerId] === 'invincible' && drunk !== playerId) invincible = playerId
      }
      if (db[room].roles[drunk] === 'don') mafiaShot = null

      if (sniperShot) {
        if (db[room].sniperShots[sniper] <= 0) sniperShot = null
        else --db[room].sniperShots[sniper]
        if (sniperShot && sniper === drunk) sniperShot = sniper
      }

      if (sniperShot && sniperShot === mafiaShot) {
        if (sniperShot === invincible) {
          sniperShot = null
          mafiaShot = null
        } else sniperShot = null
      } else {
        if (mafiaShot === docProtected || mafiaShot === invincible) mafiaShot = null
        if (sniperShot === docProtected || sniperShot === invincible) sniperShot = null
      }

      if (silencerShot && silencer === drunk) {
        silencerShot = silencer
        db[room].selections['silencer'] = silencer
      }

      if (detective) {
        const detectiveAsked = db[room].selections['detective']
        const resultRole = rolesMeta[db[room].roles[detectiveAsked]]
        let isBad = (resultRole.team === 'mafia') && !resultRole.stealth
        if (detective === drunk) isBad = !isBad
        const answer = `بازیکن ${db[room].players[detectiveAsked]} دارای نقش <b>${isBad ? 'شرور' : 'خوب'}</b> است.`
        db[room].ctx[detective].replyWithHTML(answer)
      }

      nightLog = 'وقایع شب:\n'
      if (mafiaShot) nightLog += `\nمافیا ${db[room].players[mafiaShot]} را کشت.`
      if (sniperShot) nightLog += `\nاسنایپر ${db[room].players[sniperShot]} را کشت.`
      if (silencerShot) nightLog += `\nناتاشا ${db[room].players[silencerShot]} را ساکت کرد.`
      if (!mafiaShot && !sniperShot && !silencerShot) nightLog += '\nهیچ اتفاقی نیفتاد.'

      Object.keys(db[room].players).forEach((id) => {
        removeChoices(db[room].ctx[id], nightLog)
        if (id != mafiaShot && id != sniperShot) {
          db[room].ctx[id].scene.enter('discussion')
        }
      })
      db[room].dead.forEach((id) => {
        ctx.telegram.sendMessage(id, `گزارش وقایع شب برای ارواح اتاق ${room}:\n\n${nightLog}`)
      })

      const toKill = []
      if (mafiaShot) toKill.push(mafiaShot)
      if (sniperShot) toKill.push(sniperShot)
      kill(room, toKill)
    }
  }
})

// Terror Room
const terrorRoom = new Scene('terror')
terrorRoom.enter((ctx) => {
  ctx.replyWithHTML(`<b>اتاق ترور</b>\n\nتو تروریست بودی و با رای اکثریت داری می‌میری. یک نفر رو انتخاب کن که با خودت بکشی.`)
  choose(ctx, ctx.from.id, 'others')
})
terrorRoom.on('message', (ctx) => {
  const room = ctx.session.room
  vote = {}
  if (applyChoice(ctx, ctx.message.text, vote)) {
    removeChoices(ctx, 'اوکی.')
    const victim = Object.values(vote)[0]
    const terrorLog = `بازیکن ${db[room].players[victim]} ترور شد.`
    Object.keys(db[room].players).forEach((id) => {
      db[room].ctx[id].reply(terrorLog)
    })
    db[room].dead.forEach((id) => {
      ctx.telegram.sendMessage(id, `گزارش ترور برای ارواح اتاق ${room}:\n\n${terrorLog}`)
    })
    kill(room, [ctx.from.id.toString(), victim])
  }
})

// Create scene manager
const stage = new Stage()
stage.register(waitingRoom)
stage.register(discussionRoom)
stage.register(voteRoom)
stage.register(nightRoom)
stage.register(terrorRoom)
stage.command('leave', (ctx) => {
  const room = ctx.session.room
  if (room) {
    delete db[room].players[ctx.from.id]
    delete db[room].ctx[ctx.from.id]
    delete db[room].roles[ctx.from.id]
  }
  if (db[room] && Object.keys(db[room].players).length === 0) {
    delete db[room]
  }
  ctx.session.room = null
  ctx.reply('از اتاق بازی اومدی بیرون.')
  ctx.scene.leave()
})

const bot = new Telegraf(process.env.BOT_TOKEN)
bot.use(session())
bot.use(stage.middleware())
bot.start((ctx) => {
  const args = ctx.message.text.split(' ')
  ctx.reply('به بات بازی‌گردان مافیا خوش اومدی!')
  if (args.length > 1 && args[1].startsWith('join_')) {
    const room = args[1].split('_')[1]
    join(ctx, room)
  } else {
    ctx.reply('از دستور /create برای ساخت اتاق و از دستور /join برای جوین شدن به اتاق استفاده کن.')
  }
})
bot.command('create', async (ctx) => {
  let room = ('0000' + Math.round(Math.random() * 10000)).slice(-4)
  while (room in db) {
    room = ('0000' + Math.round(Math.random() * 10000)).slice(-4)
  }
  db[room] = getRoomTemplate()
  let name = ctx.from.first_name
  if (ctx.from.last_name) name += ` ${ctx.from.last_name}`
  db[room].players[ctx.from.id] = name
  db[room].ctx[ctx.from.id] = ctx
  db[room].admin = ctx.from.id
  ctx.session.room = room
  ctx.reply(`اتاق ${room} ساخته شد.\nلینک برای جوین شدن:\nhttps://t.me/${(await ctx.telegram.getMe()).username}?start=join_${room}`)
  ctx.reply(`به عنوان ادمین اتاق، با دستور‌های /add و /remove نقش‌های بازی رو تنظیم کن و بعد به بقیه بگو به اتاق جوین شن و بعد با دستور /begin بازی رو شروع کن.\nنقش‌های موجود:\n${Object.values(rolesMeta).map(role => role.name).join('، ')}`, Markup.keyboard(
    Object.values(rolesMeta).map((role) => `/add ${role.name}`)
  ).extra())
  ctx.scene.enter('waiting')
})
bot.command('join', (ctx) => {
  const args = ctx.message.text.split(' ')
  if (args.length < 2) {
    ctx.reply('داری اشتباه می‌زنی. برای مثال اگه می‌خوای به اتاق 1111 جوین شی، این دستور رو به شکل /join 1111 استفاده کن.')
    return
  }
  const room = args[1]
  join(ctx, room)
})
bot.on('message', (ctx) => {
  const room = ctx.session.room
  if (room in db) {
    db[room].dead.forEach((id) => {
      if (id !== ctx.from.id.toString()) {
        ctx.forwardMessage(id, ctx.chat.id, ctx.message.message_id)
      }
    })
  } else {
    ctx.reply('با کی حرف می‌زنی؟')
  }
})
bot.startPolling()

process.on('unhandledRejection', (reason, promise) => {
  console.error('Unhandled Rejection at:', reason.stack || reason)
})
